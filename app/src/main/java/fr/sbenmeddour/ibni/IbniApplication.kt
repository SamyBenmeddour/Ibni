package fr.sbenmeddour.ibni

import android.app.Application
import fr.sbenmeddour.ibni.data.injection.data
import fr.sbenmeddour.ibni.domain.injection.domain
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class IbniApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@IbniApplication)
            modules(listOf(domain, data))
        }
    }
}