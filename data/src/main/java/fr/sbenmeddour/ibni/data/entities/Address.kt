package fr.sbenmeddour.ibni.data.entities

data class Address(val city: String? = null,
                   val postalCode: Long? = null,
                   val street: String? = null,
                   val streetNumber: Long? = null,
                   val additionalInformation: String? = null)