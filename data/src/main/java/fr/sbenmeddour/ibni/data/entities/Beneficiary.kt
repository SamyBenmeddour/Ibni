package fr.sbenmeddour.ibni.data.entities

import java.time.LocalDateTime

data class Beneficiary(val id: Long? = null,
                       val active: Boolean? = null,
                       val firstName: String? = null,
                       val lastName: String? = null,
                       val birthDate: LocalDateTime? = null,
                       val joinDate: LocalDateTime? = null,
                       val address: Address? = null,
                       val situation: Situation? = null,
                       val contactDetails: ContactDetails? = null)