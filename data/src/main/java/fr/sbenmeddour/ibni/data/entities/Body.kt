package fr.sbenmeddour.ibni.data.entities

data class Body(val userName: String? = null, val password: String? = null)