package fr.sbenmeddour.ibni.data.entities

import java.time.LocalDateTime

data class Body1 (val beneficiaryId: Long? = null,
                  val beginDate: LocalDateTime? = null,
                  val endDate: LocalDateTime? = null,
                  val photo: String? = null,
                  val type: String? = null)