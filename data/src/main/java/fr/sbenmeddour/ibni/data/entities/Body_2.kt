package fr.sbenmeddour.ibni.data.entities

import java.time.LocalDateTime

data class Body2 (val type: String? = null,
                  val beginDate: LocalDateTime? = null,
                  val endDate: LocalDateTime? = null,
                  val photo: String? = null)