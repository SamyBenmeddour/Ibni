package fr.sbenmeddour.ibni.data.entities

data class ContactDetails(val laneLine: Long? = null,
                          val mobile: Long? = null,
                          val email: String? = null)