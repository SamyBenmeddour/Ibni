package fr.sbenmeddour.ibni.data.entities

import java.time.LocalDateTime

data class Distribution(val id: Long? = null,
                        val beneficiaryId: Long? = null,
                        val userId: Long? = null,
                        val date: LocalDateTime? = null,
                        val size: Size? = null,
                        val isDelivery: Boolean? = null)