package fr.sbenmeddour.ibni.data.entities

import java.time.LocalDateTime

data class Document(val id: Int? = null,
                    val type: String? = null,
                    val link: String? = null,
                    val startDate: LocalDateTime? = null,
                    val endDate: LocalDateTime? = null)