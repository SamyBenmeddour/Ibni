package fr.sbenmeddour.ibni.data.entities

enum class Size { XS, S, M, L, XL }