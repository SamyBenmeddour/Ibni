package fr.sbenmeddour.ibni.data.entities

data class User(val id: Int? = null,
                val userName: String? = null,
                val firstName: String? = null,
                val lastName: String? = null,
                val email: String? = null,
                val password: String? = null,
                val accountType: AccountType? = null,
                val roles: Roles? = null,
                val gender: Gender? = null)

enum class AccountType { ADMIN, REGULAR, GUEST }

enum class Roles { PRESIDENT, TRESORIER, SECRETAIRE, BENEVOLE }

enum class Gender { MALE, FEMALE }