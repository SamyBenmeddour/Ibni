package fr.sbenmeddour.ibni.data.implementation

import fr.sbenmeddour.ibni.domain.entities.*
import fr.sbenmeddour.ibni.domain.exceptions.BeneficiaryException
import fr.sbenmeddour.ibni.domain.exceptions.RepositoryException
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryDataSource
import java.util.*

internal class FakeBeneficiaryDataSource : BeneficiaryDataSource {

    override fun fetch(filters: BeneficiaryFilters): Result<BeneficiaryResult, BeneficiaryException> {
        return (1..10).map {
            Beneficiary(
                id = it.toString(),
                name = it.toString(),
                lastName = it.toString(),
                gender = Gender.values().random(),
                birthDate = Date(0),
                joinDate = Date(),
                isBanned = it % 2 == 0,
                comment = "",
                idDisabled = it % 2 == 0
            )
        }.run {
            Success(BeneficiaryResult(this, filters, 0, -1))
        }
    }

    override fun save(beneficiary: Beneficiary): Result<Beneficiary, RepositoryException> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}