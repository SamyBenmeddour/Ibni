package fr.sbenmeddour.ibni.data.injection

import fr.sbenmeddour.ibni.data.implementation.FakeBeneficiaryDataSource
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryDataSource
import org.koin.dsl.module

val data = module {
    factory<BeneficiaryDataSource> { FakeBeneficiaryDataSource() }
}