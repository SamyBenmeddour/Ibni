package fr.sbenmeddour.ibni.domain.entities

import java.util.Date

data class Beneficiary(val id: String,
                       val name: String,
                       val lastName: String,
                       val gender: Gender,
                       val birthDate: Date,
                       val joinDate: Date,
                       val isBanned: Boolean,
                       val comment: String,
                       val idDisabled: Boolean)