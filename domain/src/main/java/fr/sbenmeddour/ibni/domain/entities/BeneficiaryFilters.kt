package fr.sbenmeddour.ibni.domain.entities

import java.util.Date

data class BeneficiaryFilters(val name: String,
                              val birthDate: ClosedRange<Date>,
                              val registrationDate: ClosedRange<Date>)