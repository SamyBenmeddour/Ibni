package fr.sbenmeddour.ibni.domain.entities
sealed class Result<out Success, out Failure : Throwable>
data class Success<out Success>(val value: Success) : Result<Success, Nothing>()
data class Failure<out Failure : Throwable>(val reason: Failure) : Result<Nothing, Failure>()

data class BeneficiaryResult(val beneficiaries: List<Beneficiary>,
                             val filters: BeneficiaryFilters,
                             val page: Int,
                             val nextPage: Int,
                             val hasNext: Boolean = nextPage != -1)