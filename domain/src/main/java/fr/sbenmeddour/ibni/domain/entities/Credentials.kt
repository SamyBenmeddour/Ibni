package fr.sbenmeddour.ibni.domain.entities

data class Credentials (val name: String, val password: String, val confirmation: String)