package fr.sbenmeddour.ibni.domain.entities

import java.time.LocalDateTime

data class DistributionFilters(val date: ClosedRange<LocalDateTime>,
                               val sizes: List<Size>,
                               val userId: String,
                               val beneficiaryId: String)