package fr.sbenmeddour.ibni.domain.entities

import java.net.URL
import java.time.LocalDateTime

data class Event(val id: String,
                 val date: LocalDateTime,
                 val name: String,
                 val description: String,
                 val visual: URL)