package fr.sbenmeddour.ibni.domain.entities

enum class Gender { MALE, FEMALE }