package fr.sbenmeddour.ibni.domain.entities

enum class Size { XS, S, M, L, XL }