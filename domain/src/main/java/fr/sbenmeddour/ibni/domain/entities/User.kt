package fr.sbenmeddour.ibni.domain.entities

data class User(val name: String,
                val lastName: String)