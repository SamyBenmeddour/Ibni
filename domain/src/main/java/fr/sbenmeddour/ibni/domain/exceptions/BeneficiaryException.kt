package fr.sbenmeddour.ibni.domain.exceptions

sealed class BeneficiaryException(override val message: String) : Exception(message)
data class InvalidBirthDateException(override val message: String) : BeneficiaryException(message)

data class InvalidNameException(override val message: String) : Exception(message)
data class InvalidLastNameException(override val message: String) : Exception(message)
