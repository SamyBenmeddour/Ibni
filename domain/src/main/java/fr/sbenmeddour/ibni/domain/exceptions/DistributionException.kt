package fr.sbenmeddour.ibni.domain.exceptions

sealed class DistributionException(override val message: String) : Exception(message)
data class InvalidDateException(override val message: String) : DistributionException(message)