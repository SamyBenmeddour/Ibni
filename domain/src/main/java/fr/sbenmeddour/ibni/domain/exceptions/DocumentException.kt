package fr.sbenmeddour.ibni.domain.exceptions

sealed class DocumentException(override val message: String) : Exception(message)
data class DocumentNotFoundException(override val message: String) : DocumentException(message)
data class InvalidInformationException(override val message: String) : DocumentException(message)
data class UploadException(override val message: String) : DocumentException(message)
