package fr.sbenmeddour.ibni.domain.exceptions

sealed class LoginException(message: String) : Exception(message)
data class BadCredentialsException(override val message: String) : LoginException(message)
data class TechnicalException(override val message: String) : LoginException(message)