package fr.sbenmeddour.ibni.domain.exceptions

sealed class RegisterException(override val message: String) : Exception(message)
data class InvalidPasswordException(override val message: String) : Exception(message)
data class WeakPasswordException(override val message: String) : Exception(message)
data class InvalidPasswordConfirmationException(override val message: String) : Exception(message)