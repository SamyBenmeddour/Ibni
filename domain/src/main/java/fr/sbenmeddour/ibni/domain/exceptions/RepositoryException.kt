package fr.sbenmeddour.ibni.domain.exceptions

sealed class RepositoryException(override val message: String) : Exception(message)

data class ItemNotFoundException(override val message: String) : RepositoryException(message)
data class ItemNotAddedException(override val message: String) : RepositoryException(message)
data class ItemNotRemovedException(override val message: String) : RepositoryException(message)