package fr.sbenmeddour.ibni.domain.injection

import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryRepository
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryRepositoryAdapter
import fr.sbenmeddour.ibni.domain.usecases.beneficiaries.*
import fr.sbenmeddour.ibni.domain.usecases.beneficiaries.GetBeneficiaryUseCaseAdapter
import fr.sbenmeddour.ibni.domain.usecases.beneficiaries.SearchBeneficiaryUseCaseAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.module

@ExperimentalCoroutinesApi
val domain = module {
    single<BeneficiaryRepository> { BeneficiaryRepositoryAdapter( dataSource = get() ) }
    factory<GetBeneficiaryUseCase> { GetBeneficiaryUseCaseAdapter(repo = get()) }
    factory<SearchBeneficiaryUseCase> { SearchBeneficiaryUseCaseAdapter( repo = get() ) }
}