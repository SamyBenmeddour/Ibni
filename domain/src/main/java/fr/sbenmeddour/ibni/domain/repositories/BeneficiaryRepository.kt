package fr.sbenmeddour.ibni.domain.repositories

import fr.sbenmeddour.ibni.domain.entities.*
import fr.sbenmeddour.ibni.domain.exceptions.BeneficiaryException
import fr.sbenmeddour.ibni.domain.exceptions.ItemNotAddedException
import fr.sbenmeddour.ibni.domain.exceptions.RepositoryException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

internal interface BeneficiaryRepository {
    fun fetch(filters: BeneficiaryFilters)
    fun save(beneficiary: Beneficiary): Beneficiary
    fun get(): Flow<Result<BeneficiaryResult, BeneficiaryException>>
}

@FlowPreview
@ExperimentalCoroutinesApi
internal class BeneficiaryRepositoryAdapter(private val dataSource: BeneficiaryDataSource) : BeneficiaryRepository {

    private val channel = BroadcastChannel<Result<BeneficiaryResult, BeneficiaryException>>(Channel.CONFLATED)

    override fun fetch(filters: BeneficiaryFilters) {
        dataSource.fetch(filters).run {
            channel.offer(this)
        }
    }

    override fun save(beneficiary: Beneficiary): Beneficiary {
        return dataSource.save(beneficiary).run {
            when (this) {
                is Success -> this.value
                is Failure -> throw this.reason
            }
        }
    }

    override fun get(): Flow<Result<BeneficiaryResult, BeneficiaryException>> = this.channel.asFlow()
}

interface BeneficiaryDataSource {
    fun fetch(filters: BeneficiaryFilters): Result<BeneficiaryResult, BeneficiaryException>
    fun save(beneficiary: Beneficiary): Result<Beneficiary, RepositoryException>
}