package fr.sbenmeddour.ibni.domain.repositories

import fr.sbenmeddour.ibni.domain.entities.Credentials
import fr.sbenmeddour.ibni.domain.entities.User
import fr.sbenmeddour.ibni.domain.exceptions.ItemNotAddedException
import fr.sbenmeddour.ibni.domain.exceptions.ItemNotFoundException
import fr.sbenmeddour.ibni.domain.exceptions.LoginException

interface UserRepository {

    @Throws(LoginException::class)
    fun find(credentials: Credentials): User

    @Throws(ItemNotAddedException::class)
    fun add(credentials: Credentials, user: User): User

}