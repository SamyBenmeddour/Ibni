package fr.sbenmeddour.ibni.domain.usecases.beneficiaries

import fr.sbenmeddour.ibni.domain.entities.*
import fr.sbenmeddour.ibni.domain.exceptions.ItemNotFoundException
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import java.util.*

interface GetBeneficiaryUseCase {
    fun get(): Flow<List<Beneficiary>>
}

@ExperimentalCoroutinesApi
internal class GetBeneficiaryUseCaseAdapter(private val repo: BeneficiaryRepository) : GetBeneficiaryUseCase {

    override fun get(): Flow<List<Beneficiary>> {
        return repo.get()
            .map {
                when (it) {
                    is Success -> it.value
                    is Failure -> throw ItemNotFoundException(it.reason.message)
                }
            }
            .scanReduce { accumulator, value ->
                if (accumulator.filters == value.filters) {
                    value.copy(beneficiaries = accumulator.beneficiaries + value.beneficiaries)
                } else {
                    value
                }
            }
            .map {
                it.beneficiaries
            }
    }

}