package fr.sbenmeddour.ibni.domain.usecases.beneficiaries

import fr.sbenmeddour.ibni.domain.entities.Beneficiary
import fr.sbenmeddour.ibni.domain.exceptions.BeneficiaryException
import fr.sbenmeddour.ibni.domain.exceptions.InvalidLastNameException
import fr.sbenmeddour.ibni.domain.exceptions.InvalidNameException
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryRepository

interface SaveBeneficiaryUseCase {
    @Throws(BeneficiaryException::class)
    fun add(beneficiary: Beneficiary): Beneficiary
}

internal class SaveBeneficiaryUseCaseAdapter(private val repo: BeneficiaryRepository) : SaveBeneficiaryUseCase {

    override fun add(beneficiary: Beneficiary): Beneficiary {
        if (beneficiary.name.isBlank()) {
            throw InvalidNameException("Name cannot be empty or blank")
        }
        if (beneficiary.lastName.isBlank()) {
            throw InvalidLastNameException("Last name cannot be empty")
        }
        return repo.save(beneficiary)
    }

}