package fr.sbenmeddour.ibni.domain.usecases.beneficiaries

import fr.sbenmeddour.ibni.domain.entities.*
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryDataSource
import fr.sbenmeddour.ibni.domain.repositories.BeneficiaryRepository
import java.util.*

interface SearchBeneficiaryUseCase {
    fun getNextPage()
    fun applyFilters(filters: BeneficiaryFilters)
}

internal class SearchBeneficiaryUseCaseAdapter(private val repo: BeneficiaryRepository) : SearchBeneficiaryUseCase {


    private var filters: BeneficiaryFilters = BeneficiaryFilters(name = "",
                                                                 birthDate = Date(0)..Date(),
                                                                 registrationDate =  Date(0)..Date())

    override fun applyFilters(filters: BeneficiaryFilters) {
        this.filters = filters
        this.repo.fetch(filters)
    }

    override fun getNextPage() {
        this.repo.fetch(this.filters)
    }

}