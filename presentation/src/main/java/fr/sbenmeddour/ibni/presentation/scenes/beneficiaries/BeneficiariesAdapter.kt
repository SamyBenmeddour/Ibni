package fr.sbenmeddour.ibni.presentation.scenes.beneficiaries

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import fr.sbenmeddour.ibni.domain.entities.Beneficiary
import fr.sbenmeddour.ibni.presentation.databinding.BeneficiaryItemBinding
import fr.sbenmeddour.ibni.presentation.utils.DataBindingViewHolder

class BeneficiariesAdapter : ListAdapter<Beneficiary, DataBindingViewHolder<Beneficiary>>(BeneficiariesCallback()) {

    override fun onBindViewHolder(holder: DataBindingViewHolder<Beneficiary>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBindingViewHolder<Beneficiary> {
        val inflater = LayoutInflater.from(parent.context)
        return BeneficiaryItemBinding.inflate(inflater).run {
            DataBindingViewHolder(this)
        }
    }
}

class BeneficiariesCallback : DiffUtil.ItemCallback<Beneficiary>() {

    override fun areContentsTheSame(oldItem: Beneficiary, newItem: Beneficiary) = oldItem == newItem

    override fun areItemsTheSame(oldItem: Beneficiary, newItem: Beneficiary) = oldItem.id == newItem.id
}