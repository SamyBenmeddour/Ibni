package fr.sbenmeddour.ibni.presentation.scenes.beneficiaries

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.sbenmeddour.ibni.domain.entities.Beneficiary


@BindingAdapter("beneficiaries")
fun RecyclerView.beneficiaries(beneficiaries: List<Beneficiary>?) {
    if (this.adapter == null) {
        this.adapter = BeneficiariesAdapter()
    }
    (this.adapter as? BeneficiariesAdapter)?.submitList(beneficiaries)
}