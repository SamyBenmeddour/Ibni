package fr.sbenmeddour.ibni.presentation.scenes.beneficiaries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import fr.sbenmeddour.ibni.presentation.R
import fr.sbenmeddour.ibni.presentation.databinding.BeneficiariesViewBinding

class BeneficiariesFragment : Fragment() {

    private val viewModel: BeneficiariesViewModel by navGraphViewModels(R.id.nav_graph) {
        BeneficiariesViewModelProvider(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return BeneficiariesViewBinding.inflate(inflater).run {
            this.lifecycleOwner = this@BeneficiariesFragment
            this.viewModel = this@BeneficiariesFragment.viewModel
            this.root
        }
    }

    private class BeneficiariesViewModelProvider(private val fragment: BeneficiariesFragment) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return modelClass.getDeclaredConstructor(BeneficiariesRouter::class.java)
                .newInstance(BeneficiariesRouterAdapter(fragment))
        }

    }

}