package fr.sbenmeddour.ibni.presentation.scenes.beneficiaries

import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

interface BeneficiariesRouter {
    fun goToFilters()
}

internal class BeneficiariesRouterAdapter(fragment: BeneficiariesFragment) : BeneficiariesRouter {

    private val navigationController: NavController by lazy { fragment.findNavController() }

    override fun goToFilters() {
        BeneficiariesFragmentDirections.actionBeneficiariesToBeneficiariesFilters().also {
            navigationController.navigate(it)
        }
    }

}