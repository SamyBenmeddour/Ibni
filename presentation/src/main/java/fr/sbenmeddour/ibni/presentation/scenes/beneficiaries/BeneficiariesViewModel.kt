package fr.sbenmeddour.ibni.presentation.scenes.beneficiaries

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import fr.sbenmeddour.ibni.domain.usecases.beneficiaries.GetBeneficiaryUseCase
import fr.sbenmeddour.ibni.domain.usecases.beneficiaries.SearchBeneficiaryUseCase
import kotlinx.coroutines.Dispatchers
import org.koin.core.KoinComponent
import org.koin.core.inject

class BeneficiariesViewModel(private val router: BeneficiariesRouter) : ViewModel(), KoinComponent {

    private val getBeneficiaries: GetBeneficiaryUseCase by inject()

    private val searchBeneficiaries: SearchBeneficiaryUseCase by inject()

    fun goToFilters() = router.goToFilters()

    val beneficiaries = getBeneficiaries.get()
        .asLiveData(viewModelScope.coroutineContext + Dispatchers.IO)

}