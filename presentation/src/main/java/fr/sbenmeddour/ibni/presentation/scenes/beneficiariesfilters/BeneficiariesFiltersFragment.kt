package fr.sbenmeddour.ibni.presentation.scenes.beneficiariesfilters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fr.sbenmeddour.ibni.presentation.databinding.BeneficiariesFiltersViewBinding

class BeneficiariesFiltersFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return BeneficiariesFiltersViewBinding.inflate(inflater).root
    }

}