package fr.sbenmeddour.ibni.presentation.scenes.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import fr.sbenmeddour.ibni.presentation.R
import kotlinx.android.synthetic.main.root_view.*

class RootActivity : AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.root_view)
        supportFragmentManager.findFragmentById(R.id.navHost)
            ?.let {
                NavigationUI.setupWithNavController(navigation, it.findNavController())
            }
    }
}