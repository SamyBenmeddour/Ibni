package fr.sbenmeddour.ibni.presentation.utils

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import fr.sbenmeddour.ibni.presentation.BR

open class DataBindingViewHolder<Type>(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Type) {
        binding.setVariable(BR.item, item)
        binding.executePendingBindings()
    }

}